# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Departments'
        db.create_table('departments', (
            ('dept_no', self.gf('django.db.models.fields.CharField')(max_length=4, primary_key=True)),
            ('dept_name', self.gf('django.db.models.fields.CharField')(max_length=40)),
        ))
        db.send_create_signal('employees', ['Departments'])

        # Adding model 'Employees'
        db.create_table('employees', (
            ('emp_no', self.gf('django.db.models.fields.IntegerField')(primary_key=True)),
            ('birth_date', self.gf('django.db.models.fields.DateField')()),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=14)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=16)),
            ('gender', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('hire_date', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal('employees', ['Employees'])

        # Adding model 'DeptEmp'
        db.create_table('dept_emp', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('emp_no', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['employees.Employees'], db_column='emp_no')),
            ('dept_no', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['employees.Departments'], db_column='dept_no')),
            ('from_date', self.gf('django.db.models.fields.DateField')()),
            ('to_date', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal('employees', ['DeptEmp'])

        # Adding model 'DeptManager'
        db.create_table('dept_manager', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('emp_no', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['employees.Employees'], db_column='emp_no')),
            ('dept_no', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['employees.Departments'], db_column='dept_no')),
            ('from_date', self.gf('django.db.models.fields.DateField')()),
            ('to_date', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal('employees', ['DeptManager'])

        # Adding model 'Salaries'
        db.create_table('salaries', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('emp_no', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['employees.Employees'])),
            ('salary', self.gf('django.db.models.fields.IntegerField')()),
            ('from_date', self.gf('django.db.models.fields.DateField')()),
            ('to_date', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal('employees', ['Salaries'])

        # Adding model 'Title'
        db.create_table('titles', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('emp_no', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['employees.Employees'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('from_date', self.gf('django.db.models.fields.DateField')()),
            ('to_date', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal('employees', ['Title'])


    def backwards(self, orm):
        # Deleting model 'Departments'
        db.delete_table('departments')

        # Deleting model 'Employees'
        db.delete_table('employees')

        # Deleting model 'DeptEmp'
        db.delete_table('dept_emp')

        # Deleting model 'DeptManager'
        db.delete_table('dept_manager')

        # Deleting model 'Salaries'
        db.delete_table('salaries')

        # Deleting model 'Title'
        db.delete_table('titles')


    models = {
        'employees.departments': {
            'Meta': {'object_name': 'Departments', 'db_table': "'departments'"},
            'dept_name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'dept_no': ('django.db.models.fields.CharField', [], {'max_length': '4', 'primary_key': 'True'})
        },
        'employees.deptemp': {
            'Meta': {'object_name': 'DeptEmp', 'db_table': "'dept_emp'"},
            'dept_no': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['employees.Departments']", 'db_column': "'dept_no'"}),
            'emp_no': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['employees.Employees']", 'db_column': "'emp_no'"}),
            'from_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'to_date': ('django.db.models.fields.DateField', [], {})
        },
        'employees.deptmanager': {
            'Meta': {'object_name': 'DeptManager', 'db_table': "'dept_manager'"},
            'dept_no': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['employees.Departments']", 'db_column': "'dept_no'"}),
            'emp_no': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['employees.Employees']", 'db_column': "'emp_no'"}),
            'from_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'to_date': ('django.db.models.fields.DateField', [], {})
        },
        'employees.employees': {
            'Meta': {'object_name': 'Employees', 'db_table': "'employees'"},
            'birth_date': ('django.db.models.fields.DateField', [], {}),
            'department': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'employee'", 'symmetrical': 'False', 'through': "orm['employees.DeptEmp']", 'to': "orm['employees.Departments']"}),
            'emp_no': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '14'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'hire_date': ('django.db.models.fields.DateField', [], {}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'manager': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'manager'", 'symmetrical': 'False', 'through': "orm['employees.DeptManager']", 'to': "orm['employees.Departments']"})
        },
        'employees.salaries': {
            'Meta': {'object_name': 'Salaries', 'db_table': "'salaries'"},
            'emp_no': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['employees.Employees']"}),
            'from_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'salary': ('django.db.models.fields.IntegerField', [], {}),
            'to_date': ('django.db.models.fields.DateField', [], {})
        },
        'employees.title': {
            'Meta': {'object_name': 'Title', 'db_table': "'titles'"},
            'emp_no': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['employees.Employees']"}),
            'from_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'to_date': ('django.db.models.fields.DateField', [], {})
        }
    }

    complete_apps = ['employees']
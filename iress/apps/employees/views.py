# Create your views here.

from datetime import date

from django.shortcuts import render_to_response
from django.views.generic import TemplateView,FormView

from django.utils.decorators import method_decorator
from django.core.context_processors import csrf
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse

from apps.employees.models import *
from apps.employees.forms import *
from apps.employees.sql import sql_employees_info

class Profile(TemplateView):
    template_name = 'employees/profile.html'

    def get_context_data(self, **kwargs):
        context = super(Profile,self).get_context_data(**kwargs)

        if self.request.user.is_superuser:
            if 'emp_id' in kwargs:
                employee = Employees.objects.get(emp_no = kwargs['emp_id'])
                context['profile'] = employee
                context['salary'] = employee.salaries_set.filter(to_date__gte = date.today())[0].salary
                context['title'] = employee.title_set.filter(to_date__gte = date.today())[0].title
                return context

        # Check to see if the emp_id has been specified.
        if 'emp_id' in kwargs:
            employee = Employees.objects.get(emp_no = kwargs['emp_id'])
            req_employee = self.request.user.employees_set.get()
            if req_employee.is_manager_of_employee(employee = employee):
                context['profile'] = employee
                context['salary'] = employee.salaries_set.filter(to_date__gte = date.today())[0].salary
                context['title'] = employee.title_set.filter(to_date__gte = date.today())[0].title

        else:
            # If emp_id is not specified return the profile for this user
            employee =  self.request.user.employees_set.get()
            context['profile'] = employee
            context['salary'] = employee.salaries_set.filter(to_date__gte = date.today())[0].salary
            context['title'] = employee.title_set.filter(to_date__gte = date.today())[0].title

        return context

    @method_decorator(login_required) #this view requires the user to be logged into the site
    def dispatch(self, *args, **kwargs):
        return super(Profile, self).dispatch(*args, **kwargs)

class Manager(FormView):
    template_name = 'employees/manager.html'
    form_class = EmployeeFilter

    def get_context_data(self, **kwargs):
        context = super(Manager,self).get_context_data(**kwargs)
        if self.request.user.is_superuser:
            employees_list = sql_employees_info(date_gte = date.today(), limit = 500, as_dict = True)
            context['dept_employees'] = {'All Employees':employees_list}
        elif self.request.user.employees_set.get().is_manager():
            employees_list = self.request.user.employees_set.get().manager_info(limit = 500)
            context['dept_employees'] = employees_list
        return context

    def form_valid(self,form):
        form = self.form_class(self.request.POST)
        if form.is_valid():
            filter_dict = form.cleaned_data
            if self.request.user.is_superuser:
                employees_list = {'All Employees': sql_employees_info(date_gte = date.today(), limit = 100, filter_dict=filter_dict, as_dict = True)}
            elif self.request.user.employees_set.get().is_manager():
                employees_list = self.request.user.employees_set.get().manager_info(filter_dict=filter_dict, limit = 500)

            context_dict = {}
            context_dict.update(csrf(self.request))
            context_dict['form'] = self.form_class(form.cleaned_data)
            context_dict['dept_employees'] = employees_list
            context = RequestContext(self.request,context_dict)
            self.success_url = reverse('manager-url')
            return render_to_response('employees/manager.html',context)
        else:
            pass
        return super(Manager,self).form_valid(form)

    @method_decorator(login_required) #this view requires the user to be logged into the site
    def dispatch(self, *args, **kwargs):
        return super(Manager, self).dispatch(*args, **kwargs)

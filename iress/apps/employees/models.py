from django.db import models
from django.contrib.auth.models import User

from datetime import date

from apps.employees.utils import yr_diff

# Create your models here.
class Departments(models.Model):
    """
    Departments are collections of employees delivering a given business function.
    """
    dept_no = models.CharField(primary_key = True, max_length = 4, verbose_name="Department Number")
    dept_name = models.CharField(max_length = 40, verbose_name="Department Name")

    class Meta:
        db_table = 'departments'

    def __unicode__(self):
        return "<dept_no: " + self.dept_no + " ; dept_name: " + self.dept_name + ">"


class Employees(models.Model):
    """
    Employees are, as their name suggests, employees of the organisation. Employees are assigned to departments,
        possibly multiple; and employees may also be managers of departments, possibly multiple.
    """


    emp_no = models.IntegerField(primary_key = True, verbose_name = "Employee Number")
    birth_date = models.DateField(verbose_name = "Birth Date")
    first_name = models.CharField(max_length=14, verbose_name = "First Name")
    last_name = models.CharField(max_length=16, verbose_name = "Last Name")
    gender = models.CharField(max_length=1, choices=(("M","Male"),("F","Female")), verbose_name = "Gender")
    hire_date = models.DateField(verbose_name = "Hire Date")
    department = models.ManyToManyField(Departments, related_name="employee", through="DeptEmp" ,verbose_name="Department")
    manager = models.ManyToManyField(Departments, related_name="manager", through="DeptManager", verbose_name="Manager")

    user = models.ForeignKey(User, null=True)

    def is_employed(self, date_gte = date.today()):
        """
        This function confirms whether a given employee is employed on or after the specified date (date_gte). It does
        this by checking that they are still receiving a salary.

        Parameters:
        -----------
        date_gte: datetime.date: the date on or after which this function checks the employee is still receiving a salary.
        """
        return self.salaries_set.filter(to_date__gte = date_gte).exists()

    def is_manager(self, date_gte = date.today()):
        """
        Returns true if the employee is a manager of a department, on or after the specified date.

        Parameters:
        -----------
        date_gte: datetime.date: the date on or after which this function checks the employee is a manager.
        """
        return self.deptmanager_set.filter(to_date__gte = date_gte).exists()

    def is_manager_of_employee(self, date_gte = date.today(), employee = None):
        """
        Returns True if the employee is a manager of the specified employee, else false.

        Parameters:
        -----------
        date_gte: datetime.date: the date on or after which this function checks the employee is a manager
        employee: Employee object: we are checking whether this employee is managed by self.
        """

        if employee is None:
            return False

        # Check this employee is at least a manager:
        if self.is_manager():
            # Get a list of all the departments the employee works in (on or after the specified date)
            # TODO: handle the case where the employee is assigned to multiple departments. Simplify this to one query.
            if employee.deptemp_set.filter(to_date__gte = date_gte).exists():
                dept = employee.deptemp_set.filter(to_date__gte = date_gte)[0].department
                # Retrieve the managers mapping to this department
                managers = dept.deptmanager_set.filter(to_date__gte = date_gte).all()
                # Check and return whether this employee is in the filtered manager set:
                return self.deptmanager_set.get() in managers
            else:
                return False
        else:
            # Employee is not a manager at all
            return False

    def manager_info(self, date_gte = date.today(), filter_dict = {}, limit = 100):
        """
        Retrieves which departments are managed by this employee, and the employees of each department managed.

        Parameters:
        -----------
        data_gte: the date on or after which the employee was a manager (if at all).
        limit: the maximum number of employees to return that are within the department (if any) managed by this employee.
        filter_dict: dictionary: filter dictionary can provide the following keys for filters. These values are used to
            filter the list of employees of each department managed by this manager.
                - first_name
                - last_name
                - gender
                - title
                - hire_lte
                - hire_gte

        Returns:
        --------
        department_managed => [employees of said department] dictionary.
        """
        # Get all the departments that the employee is still managing.
        managed_departments = self.deptmanager_set.filter(to_date__gte = date_gte)
        dept_employees = {}
        from apps.employees.sql import sql_manager_info, sql_filtered_manager_info
        for dept in managed_departments:
            # Get the current employees of the department. This gets the related model manager which we can
            # iterate through to get the employees.

            if len(filter_dict)<1:
                # If no filters
                dept_employees[dept.department] = sql_manager_info(department = dept.department, limit = limit)
            else:
                # Apply filters:
                dept_employees[dept.department] = sql_filtered_manager_info(department = dept.department,
                                            date_gte = date_gte, limit = limit,
                                            filter_dict = filter_dict, as_dict = True)

        return dept_employees

    def age(self):
        """
        Returns the age of the employee in years
        """
        return yr_diff(date.today(),self.birth_date)

    def yrs_with_company(self):
        """
        Returns the number of years the employee has been with the company.
        """
        return yr_diff(date.today(),self.hire_date)

    class Meta:
        db_table = 'employees'

    def __unicode__(self):
        return "<emp_no: " + str(self.emp_no) + " ; first_name: " + self.first_name + " ; last_name: " + self.last_name + ">"

class DeptEmp(models.Model):
    """
    This class maps Employees to Departments as employees..
    """

    employee = models.ForeignKey(Employees, db_column = "emp_no")
    department = models.ForeignKey(Departments, db_column = "dept_no")
    to_date = models.DateField()
    from_date = models.DateField()

    class Meta:
        db_table = 'dept_emp'

    def __unicode__(self):
        return "<emp_no: " + str(self.employee.emp_no) + "; dept_no: " + self.department.dept_no + \
               " ; from_date: " + self.from_date.isoformat() + " ; to_date: " + self.to_date.isoformat() + ">"

class DeptManager(models.Model):
    """
    This class maps Employees to Departments as managers.
    """

    employee = models.ForeignKey(Employees, db_column = "emp_no")
    department = models.ForeignKey(Departments, db_column = "dept_no")
    from_date = models.DateField()
    to_date = models.DateField()

    class Meta:
        db_table = 'dept_manager'

    def __unicode__(self):
        return "<emp_no: " + str(self.employee.emp_no) + "; dept_no: " + self.department.dept_no +\
               " ; from_date: " + self.from_date.isoformat() + " ; to_date: " + self.to_date.isoformat() + ">"

class Salaries(models.Model):
    """
    The salary (per year?) of a specified employee during a given interval of time.
    """

    employee = models.ForeignKey(Employees, verbose_name = "Employee Number", db_column = "emp_no")
    salary = models.IntegerField(verbose_name = "Salary")
    from_date = models.DateField(verbose_name = "From")
    to_date = models.DateField(verbose_name = "Until")

    class Meta:
        db_table = 'salaries'

    def __unicode__(self):
        return "<emp_no: " + str(self.employee.emp_no) + " ; salary: " + str(self.salary) + \
               " ; from_date: " + self.from_date.isoformat() + " ; to_date: " + self.to_date.isoformat() + ">"

class Title(models.Model):
    """
    The title of a given employee during a given interval of time.
    """
    id = models.AutoField(primary_key=True, db_column = "id")
    employee = models.ForeignKey(Employees, verbose_name = "Employee Number", db_column = "emp_no")
    title = models.CharField(max_length=50, verbose_name = "Title")
    from_date = models.DateField(verbose_name = "From")
    to_date = models.DateField(verbose_name = "Until")

    class Meta:
        db_table = 'titles'


    def __unicode__(self):
        return "<emp_no: " + str(self.employee.emp_no) + " ; title: " + self.title +\
               " ; from_date: " + self.from_date.isoformat() + " ; to_date: " + self.to_date.isoformat() + ">"


from datetime import date

def yr_diff(date_one, date_two):
    """
    This function calculates the "Human" year difference between two dates.
    """
    if date_one > date_two:
        high_date = date_one
        low_date = date_two
    else:
        high_date = date_two
        low_date = date_one

    if (low_date.day == high_date.day) and (low_date.month == high_date.month):
        return high_date.year - low_date.year
    elif date(high_date.year,low_date.month, low_date.day) > high_date:
        return high_date.year - low_date.year -1
    else:
        return high_date.year - low_date.year
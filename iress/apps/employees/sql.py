__author__ = 'werner'

from django.db import connection
from datetime import date

def dictfetchall(cursor):
    "Returns all rows from a cursor as a dict"
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]

def sql_manager_info(department = None, date_gte = date.today(), limit = 100, as_dict = True):
    """
    Get the employee summary information, for all employees of the specified department.
    """

    if department is None:
        return

    cursor = connection.cursor()

    query = 'SELECT `employees`.`emp_no`, `employees`.`first_name`, `employees`.`last_name`, ' + \
            '`employees`.`gender`, `employees`.`hire_date`, ' + \
            '`titles`.`title` ' + \
            'FROM `dept_emp` ' + \
            'INNER JOIN `employees` ON (`dept_emp`.`emp_no` = `employees`.`emp_no`) ' +\
            'INNER JOIN `titles` ON (`employees`.`emp_no` = `titles`.`emp_no` ) ' +\
            'WHERE (`dept_emp`.`dept_no` = %s  AND `dept_emp`.`to_date` >= %s AND `titles`.`to_date` >= %s ) ' + \
            ' LIMIT %s'

    cursor.execute(query, (department.dept_no, date_gte, date_gte, limit,))

    if as_dict:
        return dictfetchall(cursor)
    else:
        return list(cursor.fetchall())

def sql_filtered_manager_info(department = None, date_gte = date.today(), limit = 100, filter_dict = {}, as_dict = True):
    """
    Get the employee summary information, for all employees of the specified department.

    Parameters:
    -----------
    department: Department object: the department to which the employees belong.
    date_gte: datetime.date: ensures that the employee is an employee of the given department on or after the specified
        date. It also ensures that the employee title retrieves is matched on or after this date.
    limit: int: the number of rows to limit the query to.
    filter_dict: dictionary: filter dictionary can provide the following keys for filters:
        - first_name
        - last_name
        - gender
        - title
        - hire_lte
        - hire_gte
    """

    if department is None:
        return

    if len(filter_dict) < 1:
        # No filters applied
        return sql_manager_info(department = department, date_gte = date_gte, limit = limit, as_dict = as_dict)

    query_params = (department.dept_no, date_gte, date_gte)
    query_filters = {}


    # Construct the query based on the applied filters.
    if filter_dict['first_name'] == '':
        query_filters['first_name'] = 1
    else:
        query_filters['first_name'] = '`employees`.`first_name` = %s'
        query_params += (filter_dict['first_name'],)

    if filter_dict['last_name'] == '':
        query_filters['last_name'] = 1
    else:
        query_filters['last_name'] = '`employees`.`last_name` = %s'
        query_params += (filter_dict['last_name'],)

    if filter_dict['gender'] == '':
        query_filters['gender'] = 1
    else:
        query_filters['gender'] = '`employees`.`gender` = %s'
        query_params += (filter_dict['gender'],)

    if filter_dict['title'] == '':
        query_filters['title'] = 1
    else:
        query_filters['title'] = '`titles`.`title` = %s'
        query_params += (filter_dict['title'],)

    if filter_dict['hire_gte'] is None:
        query_filters['hire_gte'] = 1
    else:
        query_filters['hire_gte'] = '`employees`.`hire_date` >= %s'
        query_params += (filter_dict['hire_gte'],)

    if filter_dict['hire_lte'] is None:
        query_filters['hire_lte'] = 1
    else:
        query_filters['hire_lte'] = '`employees`.`hire_date` <= %s'
        query_params += (filter_dict['hire_lte'],)



    query_params += (limit,)


    cursor = connection.cursor()

    query = 'SELECT `employees`.`emp_no`, `employees`.`first_name`, `employees`.`last_name`, ' +\
            '`employees`.`gender`, `employees`.`hire_date`, ' +\
            '`titles`.`title` ' +\
            'FROM `dept_emp` ' +\
            'INNER JOIN `employees` ON (`dept_emp`.`emp_no` = `employees`.`emp_no`) ' +\
            'INNER JOIN `titles` ON (`employees`.`emp_no` = `titles`.`emp_no` ) ' +\
            'WHERE (`dept_emp`.`dept_no` = %s  AND `dept_emp`.`to_date` >= %s AND `titles`.`to_date` >= %s AND ' + \
            '{first_name_query} AND {last_name_query} AND {gender_query} AND {title_query} AND {hire_date_gte} AND {hire_date_lte}) ' +\
            ' LIMIT %s'

    query = query.format(first_name_query = query_filters['first_name'], last_name_query = query_filters['last_name'],
                    gender_query = query_filters['gender'], title_query = query_filters['title'],
                    hire_date_gte = query_filters['hire_gte'], hire_date_lte = query_filters['hire_lte'],)

    cursor.execute(query, query_params)

    if as_dict:
        return dictfetchall(cursor)
    else:
        return list(cursor.fetchall())


def sql_employees_info(date_gte = date.today(), limit = 100, filter_dict = {}, as_dict = True):
    """
    Get the employee summary information, for all employees.

    Parameters:
    -----------
    department: Department object: the department to which the employees belong.
    date_gte: datetime.date: ensures that the employee is an employee of the given department on or after the specified
        date. It also ensures that the employee title retrieves is matched on or after this date.
    limit: int: the number of rows to limit the query to.
    filter_dict: dictionary: filter dictionary can provide the following keys for filters:
        - first_name
        - last_name
        - gender
        - title
        - hire_lte
        - hire_gte
    """

    if len(filter_dict) < 1:
        # No filters applied, so create an empty filters dict:
        filter_dict = {'first_name': '',
                       'last_name': '',
                       'gender': '',
                       'title': '',
                       'hire_lte': None,
                       'hire_gte': None}

    query_params = (date_gte, date_gte)
    query_filters = {}


    # Construct the query based on the applied filters.
    if filter_dict['first_name'] == '':
        query_filters['first_name'] = 1
    else:
        query_filters['first_name'] = '`employees`.`first_name` = %s'
        query_params += (filter_dict['first_name'],)

    if filter_dict['last_name'] == '':
        query_filters['last_name'] = 1
    else:
        query_filters['last_name'] = '`employees`.`last_name` = %s'
        query_params += (filter_dict['last_name'],)

    if filter_dict['gender'] == '':
        query_filters['gender'] = 1
    else:
        query_filters['gender'] = '`employees`.`gender` = %s'
        query_params += (filter_dict['gender'],)

    if filter_dict['title'] == '':
        query_filters['title'] = 1
    else:
        query_filters['title'] = '`titles`.`title` = %s'
        query_params += (filter_dict['title'],)

    if filter_dict['hire_gte'] is None:
        query_filters['hire_gte'] = 1
    else:
        query_filters['hire_gte'] = '`employees`.`hire_date` >= %s'
        query_params += (filter_dict['hire_gte'],)

    if filter_dict['hire_lte'] is None:
        query_filters['hire_lte'] = 1
    else:
        query_filters['hire_lte'] = '`employees`.`hire_date` <= %s'
        query_params += (filter_dict['hire_lte'],)



    query_params += (limit,)


    cursor = connection.cursor()

    query = 'SELECT `employees`.`emp_no`, `employees`.`first_name`, `employees`.`last_name`, ' +\
            '`employees`.`gender`, `employees`.`hire_date`, ' +\
            '`titles`.`title` ' +\
            'FROM `dept_emp` ' +\
            'INNER JOIN `employees` ON (`dept_emp`.`emp_no` = `employees`.`emp_no`) ' +\
            'INNER JOIN `titles` ON (`employees`.`emp_no` = `titles`.`emp_no` ) ' +\
            'WHERE (`dept_emp`.`to_date` >= %s AND `titles`.`to_date` >= %s AND ' +\
            '{first_name_query} AND {last_name_query} AND {gender_query} AND {title_query} AND {hire_date_gte} AND {hire_date_lte}) ' +\
            ' LIMIT %s'

    query = query.format(first_name_query = query_filters['first_name'], last_name_query = query_filters['last_name'],
        gender_query = query_filters['gender'], title_query = query_filters['title'],
        hire_date_gte = query_filters['hire_gte'], hire_date_lte = query_filters['hire_lte'],)

    cursor.execute(query, query_params)

    if as_dict:
        return dictfetchall(cursor)
    else:
        return list(cursor.fetchall())

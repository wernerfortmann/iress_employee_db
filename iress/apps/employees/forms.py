__author__ = 'werner'

from django import forms

class EmployeeFilter(forms.Form):
    """
    This form is used to apply the employee filter on the Manager view. It allows the manager to apply a fine
    grained filter on the employees of her/his division.
    """
    first_name = forms.CharField(label='first_name', required=False)
    last_name = forms.CharField(label='last_name', required=False)
    gender = forms.ChoiceField(label='gender', choices=(('','Not Specified'),('M','Male'),('F','Female')), required=False)

    titles = (('','Not Specified'),(u'Senior Engineer',u'Senior Engineer'), (u'Staff',u'Staff'), (u'Engineer',u'Engineer'),
              (u'Senior Staff',u'Senior Staff'), (u'Assistant Engineer',u'Assistant Engineer'),
              (u'Technique Leader',u'Technique Leader'), (u'Manager',u'Manager'))
    title = forms.ChoiceField(label='title', choices=titles, required=False)

    hire_gte = forms.DateField(required=False)
    hire_lte = forms.DateField(required=False)


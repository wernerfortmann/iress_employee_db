from django.conf.urls import patterns, include, url

from apps.employees.views import *

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^profile/(?P<emp_id>\d+)$', Profile.as_view(), name='profile-spec-url'),
    url(r'^profile/$', Profile.as_view(), name='profile-url'),
    url(r'^manager/$', Manager.as_view(), name='manager-url'),


)
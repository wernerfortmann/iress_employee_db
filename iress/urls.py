from django.conf.urls import patterns, include, url

from views import *

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'views.login', name='home-url'),
    url(r'^logout/$', 'views.logout', name='logout-url'),
    url(r'^resetpassword/$', ResetPassword.as_view(), name='resetpassword-url'),

    url(r'^employee/', include('apps.employees.urls')),

    # Examples:
    # url(r'^$', 'views.home', name='home'),
    # url(r'^iress/', include('foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

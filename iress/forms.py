__author__ = 'werner'


from django import forms

class PasswordResetForm(forms.Form):
    password = forms.CharField(label='Password', widget=forms.PasswordInput(render_value=False))
    password_check = forms.CharField(label='Confirm Password', widget=forms.PasswordInput(render_value=False))

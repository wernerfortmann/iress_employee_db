__author__ = 'werner'


from django.shortcuts import render_to_response, HttpResponseRedirect
from django.views.generic import FormView
from django.core.urlresolvers import reverse
from django.db import transaction

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.core.context_processors import csrf
from django.template import RequestContext

from django.contrib.auth.models import User

from apps.employees.models import *
from forms import *

@transaction.commit_on_success()
def login(request):
    from django.contrib.auth.forms import AuthenticationForm
    from django.contrib.auth import authenticate,login

    def error_context(request):
        context_dict = {}
        context_dict.update(csrf(request))
        context_dict['form'] = AuthenticationForm
        context_dict['error'] = 'The username and/or password you entered are not valid.'
        context = RequestContext(request,context_dict)
        return render_to_response('site/home.html',context)

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        # Check if this user already exists in the database.
        if User.objects.filter(username=username).exists():
            # Use standard Django authentication.
            user = User.objects.get(username=username)

            # If user is not a superadmin, check the user is currently employed:
            if not user.is_superuser:
                employee = user.employees_set.get()
                if not employee.is_employed():
                    raise Http404('Failed to login. Please contact your system administrator.')
            user = authenticate(username=username,password=password)
            if user:
                # User is authenticated
                if user.is_active:
                    login(request,user)

                    # If password is equal to the emp_no redirect them to the password reset page.
                    if not user.is_superuser:
                        if password == str(employee.emp_no):
                            return HttpResponseRedirect('/resetpassword/')
                    else:
                        if password == 'Admin1':
                            return HttpResponseRedirect('/resetpassword/')


                    return HttpResponseRedirect('/')
                else:
                    raise Http404('Failed to login. Please contact your system administrator.')
            else:
                # User is not authenticated.
                return error_context(request)
        else:
            # Authenticate against Employees table.
            try:
                [first_name, last_name] = username.split('.')

                if Employees.objects.filter(first_name=first_name, last_name=last_name, emp_no=password,
                                                salaries__to_date__gte=date.today()).exists():
                    # Employee has the correct details, and is still employed, to now authenticate and update their password.

                    # We'll begin by creating the User model
                    emp = Employees.objects.get(first_name=first_name, last_name=last_name, emp_no=password)
                    user = User.objects.create_user(username=username,password=password)
                    user.first_name = first_name
                    user.last_name = last_name
                    user.save()
                    emp.user = user
                    emp.save()
                    user = authenticate(username=username,password=password)
                    login(request,user)
                    return HttpResponseRedirect('/resetpassword/')

                else:
                    # User is not authenticated.
                    return error_context(request)

            except ValueError:
                # Username has too many or too few '.'; or password cannot be cast to integer
                return error_context(request)
    else:
        # Request Method is Get
        context_dict = {}
        context_dict.update(csrf(request))
        context_dict['form'] = AuthenticationForm
        context = RequestContext(request,context_dict)
        return render_to_response('site/home.html',context)

def logout(request):
    from django.shortcuts import HttpResponseRedirect
    from django.contrib.auth.views import logout

    logout(request)
    return HttpResponseRedirect(reverse('home-url'))

class ResetPassword(FormView):
    """
    This view provides general analytics for a selected set of assets.
    """
    form_class = PasswordResetForm
    template_name = 'site/resetpassword.html'

    def form_valid(self,form):
        form = self.form_class(self.request.POST)
        if form.is_valid():
            new_password = form.cleaned_data['password']
            password_check = form.cleaned_data['password_check']
            if new_password != password_check:
                context_dict = {}
                context_dict.update(csrf(self.request))
                context_dict['form'] = self.form_class
                context_dict['error'] = 'The passwords you entered do not match.'
                context = RequestContext(self.request,context_dict)
                return render_to_response('site/resetpassword.html',context)
            user = self.request.user
            user.set_password(new_password)
            user.save()
        else:
            pass
        return super(ResetPassword,self).form_valid(form)

    def get_success_url(self):
        return reverse('home-url')

    @method_decorator(login_required) #this view requires the user to be logged into the site
    def dispatch(self, *args, **kwargs):
        return super(ResetPassword, self).dispatch(*args, **kwargs)